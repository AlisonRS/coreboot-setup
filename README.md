[![Coreboot](https://www.coreboot.org/images/thumb/7/75/Coreboot_full_web.png/120px-Coreboot_full_web.png)](https://www.coreboot.org/)      [![GRUB2](http://www.inf.sgsp.edu.pl/pub/MALUNKI/LOGO/grub.png)](https://www.gnu.org/software/grub/)



# x230 Coreboot Setup


Configuration and helper script for easy build, assembly and installation of Coreboot on the Thinkpad X220, X230, t420 or t430, please see the **settings.cfg** for more details.

This setup keeps the descriptor, gbe config and Intel ME from the stock BIOS, neutralizes the Intel ME by removing most of its partitions with [me_cleaner](https://github.com/corna/me_cleaner) and prepares a flash image with [Coreboot](https://www.coreboot.org/) using [SeaBIOS](https://www.seabios.org)+[GRUB](https://www.gnu.org/software/grub/) with LUKS+LVM support as the default payload

Usage:

    ./cb-helper operation [target]

Alternatively the **auto-run** script can be launched to automatically run through all the operations in order for all devices, provided a valid BIOS stock image containing the Intel ME/TXE firmware has been placed in "binaries/bios/bios.bin."target" for each target.

Optionally a GPG key can be generated and placed in the "keys/" folder which is used to sign the GRUB config and payload files, make sure to sign the kernel and initramfs with the "keys/boot.key".

Adding a GRUB password and superuser is strongly recommended

More information on hardening GRUB can be found in the [Libreboot Docs](https://libreboot.org/docs/gnulinux/grub_hardening.html)



## Configs

**settings.cfg** - Helper script configuration

**coreboot.config** - Coreboot build configuration for specific target

**seabios.config** - SeaBIOS configuration

**grub_memdisk.cfg** - GRUB configuration inside the memdisk

**grub_modules.conf** - GRUB modules, installed and loaded



## Operations

**generate_key**

Generate GPG key to sign the GRUB config and payloads


**download_code**

Download repositories for Coreboot, SeaBIOS ,GRUB and me_cleaner


**patch_projects**

Applies patches for GRUB


**build_utils**

Build useful utilities like cbfstool and idftool


**split_bios**

Splits the stock bios ("binaries/bios.bin") into descriptor, gbe configuration and Intel ME


**neuter_me**

Neutralize Intel ME by removing most of its partitions using me_cleaner


**build_seabios**

Build SeaBIOS


**pre_build_coreboot**

Copy config files and build Coreboot's toolchain


**build_coreboot**

Build Coreboot


**build_grub**

Build GRUB


**assemble_grub**

Assembles configurations and GRUB payload


**config_seabios**

Configure SeaBIOS to boot GRUB directly from the CBFS volume


**config_nvram**

Configure default CMOS Values in the CBFS volume


**install_grub**

Install the GRUB payload and configuration in the CBFS volume


**config_cbfs**

Install the secondary payloads in the CBFS volume


**clean_dir**

Clean output and binaries folder


**release**

Add rom to achieve with changelog, version and build date with checksum


**split_rom**

Split the coreboot rom into 8MB bottom chip portion and 4MB top chip portion for external flashing


## Dependencies

**Debian 9**

    sudo apt-get install git wget build-essential gnat flex bison libncurses5-dev zlib1g-dev libfreetype6-dev unifont python3
    
    sudo apt-get build-dep grub

## Attribuition

The scripts were originaly for the Thinkpad X220 by [Velsoth](https://notabug.org/Velsoth/x220-coreboot) which are released under the [GNU GPLv3](https://notabug.org/Velsoth/x220-coreboot/src/master/LICENSE.md)

resource/config/**grub.cfg**, resource/config/**grub_enforce.cfg**, resource/config/**grub_modules.conf**, resource/patches/grub/**.patch** and resource/keymaps/**.gkb** are based on their [Libreboot](https://libreboot.org/) counterparts which are released under the [GNU GPLv3](https://notabug.org/libreboot/libreboot/raw/master/COPYING)

resource/patches/grub/**0002-Cryptomount-support-key-files.patch** are by [John Lane](https://github.com/johnlane) and modified by [Andrew Robbins](https://notabug.org/and_who) work for [Libreboot](https://libreboot.org/) which are released under the [GNU GPLv3](https://github.com/johnlane/grub/blob/master/COPYING)

The font **DejaVuSans Mono** is used in GRUB and is licensed under [a free license](https://dejavu-fonts.github.io/License.html)


## Licensing

This entire project is licensed under the [GNU General Public License Version 3](https://gitlab.com/AlisonRS/x230-coreboot/blob/master/LICENSE)
